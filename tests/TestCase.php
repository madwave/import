<?php

namespace Madwave\Import\Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

/**
 * @group import
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

}
