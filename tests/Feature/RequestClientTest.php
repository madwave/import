<?php

namespace Madwave\Import\Tests\Feature;

use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;
use Madwave\Import\ApiContext;
use Madwave\Import\RequestClient;
use Madwave\Import\Tests\TestCase;
use Mockery;

/**
 * @group import
 */
class RequestClientTest extends TestCase {

	public function tearDown(): void {
		parent::tearDown();
		Mockery::close();
	}

	public function test_get_resources() {
		$url = 'https://host/api/categories';
		$params = ['locale' => 'ru'];
		$token = 'secret_token';
		$method = 'get';

		$fullUrl = $url.'?'.http_build_query($params);

		$stubContext = $this->createStub(ApiContext::class);
		$stubContext->method('getFullUrl')->willReturn($url);
		$stubContext->method('getMethod')->willReturn($method);
		$stubContext->method('getParams')->willReturn($params);
		$stubContext->method('getToken')->willReturn($token);
		$stubContext->method('setParam');

		// формат ответа api
		$responceBody = [
			'data' => [
				['prop' => 'val']
			]
		];
		Http::fake(['*' => Http::response($responceBody)]);

		$instance = new RequestClient($stubContext);
		$result = $instance->getResources(1);

		Http::assertSent(function(Request $request) use ($fullUrl, $token, $method) {
			return $request->hasHeader('Authorization', "Bearer $token")
				&& $request->method() === strtoupper($method)
				&& $request->url() === $fullUrl;
		});

		$this->assertIsArray($result);
		$this->assertCount(1, $result);
		$this->assertEquals($result[0], (object) $responceBody['data'][0]);
	}

	public function test_get_all_resources() {
		$mockInstance = Mockery::mock(RequestClient::class)->makePartial();

		$responces = [
			(object) ['first' => 10],
			(object) ['second' => 20],
		];

		$mockInstance
			->shouldReceive('getResources')
			->twice()
			->withArgs(function($page, &$meta=null) { 
				// для записи в ссылку $meta - проверки аргументов нет
				$meta = (object) ['last_page' => 2];
				return true;
			})
			->andReturn([$responces[0]], [$responces[1]]);

		$result = $mockInstance->getAllResources();

		$this->assertEquals($result, $responces);
	}
}
