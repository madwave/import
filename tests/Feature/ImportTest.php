<?php

namespace Madwave\Import\Tests\Feature;

use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;
use Madwave\Import\Tests\TestCase;

use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;

/**
 * @group import
 */
class ImportTest extends TestCase {

	public function test_is_runtime() {
		$import = new Import(ImportTypeEnum::ALL);
		config(['import.types.all_resources.runtime' => Carbon::now()->format('Y-m-d H:i:s')]);
		$this->assertTrue($import->isRuntime(), 'This is not runtime');

		config(['import.types.all_resources.runtime' => Carbon::now()->addHours(2)->format('Y-m-d H:i:s')]);
		$this->assertFalse($import->run(), 'Force is not working');
	}

	public function test_set_queue() {
		Queue::fake();

		$import = new Import(ImportTypeEnum::ALL);
		$queueName = config('import.types.' . $import->type->value . '.queue');
		$force = true;

		Queue::assertNothingPushed();

		$import->onQueue($queueName)->run($force);

		$this->assertEquals(1, Queue::size($queueName));
	}
}
