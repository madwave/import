<?php

namespace Madwave\Import\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\Models\BrandImport;
use Madwave\Import\Services\ImportService\DataImportInfo\LastImportInfoResource;
use Madwave\Import\Services\ImportService\ImportRepository;
use Madwave\Import\Tests\TestCase;
use Mockery;
use stdClass;

/**
 * @group import
 */
class ImportRepositoryTest extends TestCase {

	use RefreshDatabase;

	public function tearDown(): void {
		parent::tearDown();
		Mockery::close();
	}

	public function test_add() {

		$importInfoMock = Mockery::mock(LastImportInfoResource::class);
		$importInfoMock->shouldReceive('pushAdded')->once();

		$repository = new ImportRepository(new BrandImport(), $importInfoMock);
		$importObject = $this->getImportObject();

		$result = $repository->importAsArray([$importObject]);

		$this->assertsForImportAsArrayResult($result, added: 1, notAdded: 0);
		$this->assertDatabaseHas(BrandImport::tableName(), [
			'name' => $importObject->name,
		]);
	}

	public function test_not_add() {

		$importInfoMock = Mockery::mock(LastImportInfoResource::class);
		$importInfoMock->shouldReceive('pushMissed')->once();

		$repository = new ImportRepository(new BrandImport(), $importInfoMock);
		$importObject = $this->getImportObject();

		// импортируемая модель уже существует
		BrandImport::create((array) $importObject);

		$result = $repository->importAsArray([$importObject]);

		$this->assertsForImportAsArrayResult($result, added: 0, notAdded: 1);
	}

	public function test_change() {

		$importInfoMock = Mockery::mock(LastImportInfoResource::class);
		$importInfoMock->shouldReceive('pushAdded')->once();

		$repository = new ImportRepository(new BrandImport(), $importInfoMock);
		$importObject = $this->getImportObject();

		BrandImport::create((array) $importObject);

		$importObject->name = 'changed_name';
		$result = $repository->importAsArray([$importObject]);

		$this->assertsForImportAsArrayResult($result, added: 1, notAdded: 0);
		$this->assertDatabaseHas(BrandImport::tableName(), [
			'name' => $importObject->name,
		]);
	}

	public function test_delete() {

		$importInfoMock = Mockery::mock(LastImportInfoResource::class);
		$importInfoMock->shouldReceive('pushMissed')->once();

		$repository = new ImportRepository(new BrandImport(), $importInfoMock);
		$importObject = $this->getImportObject();

		$deleteObject = $this->getImportObject();
		$deleteObject->id = 2;
		$deleteObject->name = 'object for deleting';

		BrandImport::create((array) $importObject);
		$deletingBrand = BrandImport::create((array) $deleteObject);

		$result = $repository->importAsArray([$importObject], withDelete: true);

		$this->assertsForImportAsArrayResult($result, added: 0, notAdded: 1);
		$this->assertDatabaseHas(BrandImport::tableName(), [
			'name' => $importObject->name,
		]);
		$this->assertDeleted($deletingBrand);
	}

	private function getImportObject(): stdClass {
		return (object) [
			'id' => 1,
			'name' => 'brand_1'
		];
	}

	private function assertsForImportAsArrayResult(array $result, int $added, int $notAdded): void {
		$this->assertArrayHasKey('added', $result);
		$this->assertArrayHasKey('notAdded', $result);
		$this->assertCount($added, $result['added']);
		$this->assertCount($notAdded, $result['notAdded']);
	}
}
