<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\SecondProductResource;
use Madwave\Import\Models\ProductImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class ProductImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$product = ProductImport::factory()->make(['id' => 1]);

		$resources = SecondProductResource::collection([$product])
			->collection
			->map(fn($productResource) => (object) $productResource->resolve())
			->toArray();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new ProductImport(), $resources);

		$this->assertDatabaseHas(ProductImport::tableName(), [
			'name' => $product->name,
			'certificate_ce' => $product->certificate_ce,
			'certificate_eac' => $product->certificate_eac,
			'measure_id' => $product->measure_id,
			'description' => $product->description,
			'compositions' => $product->compositions,
			'components' => $product->components,
			'features' => $product->features,
			'youtube_links' => $product->youtube_links,
			'category_id' => $product->category_id,
			'type_id' => $product->type_id,
			'country_id' => $product->country_id,
			'brand_id' => $product->brand_id,
			'gender_id' => $product->gender_id,
		]);
	}
}