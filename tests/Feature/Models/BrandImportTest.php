<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\BrandResource;
use Madwave\Import\Models\BrandImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class BrandImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$brand = BrandImport::factory()->make(['id' => 1]);

		$resources = BrandResource::collection([$brand])->resolve();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new BrandImport(), $resources);

		$this->assertDatabaseHas(BrandImport::tableName(), [
			'name' => $brand->name,
		]);
	}
}