<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\Models\ProductSizesSizeDetailsImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class ProductSizesSizeDetailsImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$sizeDetail = ProductSizesSizeDetailsImport::factory()->make(['id' => 1]);

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new ProductSizesSizeDetailsImport(), [$sizeDetail->toArray()]);

		$this->assertDatabaseHas(ProductSizesSizeDetailsImport::tableName(), [
			'value' => $sizeDetail->value,
			'size_id' => $sizeDetail->size_id,
			'size_detail_id' => $sizeDetail->size_detail_id,
		]);
	}
}