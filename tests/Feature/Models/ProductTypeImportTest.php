<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\TypeResource;
use Madwave\Import\Models\ProductTypeImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class ProductTypeImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$type = ProductTypeImport::factory()->make(['id' => 1]);

		$resources = TypeResource::collection([$type])->resolve();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new ProductTypeImport(), $resources);

		$this->assertDatabaseHas(ProductTypeImport::tableName(), [
			'name' => $type->name,
		]);
	}
}