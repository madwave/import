<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\CategoryResource;
use Madwave\Import\Models\ProductCategoryImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class ProductCategoryImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$category = ProductCategoryImport::factory()->make(['id' => 1]);

		$resources = CategoryResource::collection([$category])->resolve();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new ProductCategoryImport(), $resources);

		$this->assertDatabaseHas(ProductCategoryImport::tableName(), [
			'name' => $category->name,
			'parent_id' => $category->parent_id,
		]);
	}
}