<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\VariantResource;
use Madwave\Import\Models\ProductPriceImport;
use Madwave\Import\Models\ProductStockImport;
use Madwave\Import\Models\ProductVariantsImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class ProductVariantsImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$variant = ProductVariantsImport::factory()
			->has(ProductPriceImport::factory()->count(1), 'prices')
			->has(ProductStockImport::factory()->count(1), 'stocks')
			->create();

		$resources = VariantResource::collection([$variant])->resolve();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new ProductVariantsImport(), $resources);

		$this->assertDatabaseHas(ProductVariantsImport::tableName(), [
			'product_id' => $variant->product_id,
			'size_id' => $variant->size_id,
			'color_id' => $variant->color_id,
			'article' => $variant->article,
			'barcode' => $variant->barcode,
			'discontinued' => $variant->discontinued,
			'gross_weight' => $variant->gross_weight,
			'net_weight' => $variant->net_weight,
			'volume' => $variant->volume,
			'length' => $variant->length,
			'width' => $variant->width,
			'height' => $variant->height,
			'carton_length' => $variant->carton_length,
			'carton_width' => $variant->carton_width,
			'carton_height' => $variant->carton_height,
			'is_new' => $variant->is_new,
		]);
	}
}