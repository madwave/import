<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\MeasureResource;
use Madwave\Import\Models\MeasureImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class MeasureImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$measure = MeasureImport::factory()->make(['id' => 1]);

		$resources = MeasureResource::collection([$measure])->resolve();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new MeasureImport(), $resources);

		$this->assertDatabaseHas(MeasureImport::tableName(), [
			'name' => $measure->name,
		]);
	}
}