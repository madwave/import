<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\GenderResource;
use Madwave\Import\Models\GenderImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class GenderImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$gender = GenderImport::factory()->make(['id' => 1]);

		$resources = GenderResource::collection([$gender])->resolve();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new GenderImport(), $resources);

		$this->assertDatabaseHas(GenderImport::tableName(), [
			'name' => $gender->name,
		]);
	}
}