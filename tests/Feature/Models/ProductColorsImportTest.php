<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\ColorResource;
use Madwave\Import\Models\ProductColorsImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class ProductColorsImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$color = ProductColorsImport::factory()->make(['id' => 1]);

		$resources = ColorResource::collection([$color])->resolve();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new ProductColorsImport(), $resources);

		$this->assertDatabaseHas(ProductColorsImport::tableName(), [
			'name' => $color->name,
			'hex' => $color->hex,
		]);
	}
}