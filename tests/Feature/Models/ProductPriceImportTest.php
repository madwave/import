<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\PriceResource;
use Madwave\Import\Models\ProductPriceImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class ProductPriceImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$priceModel = ProductPriceImport::factory()->make();

		$resources = PriceResource::collection([$priceModel])->resolve();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new ProductPriceImport(), $resources);

		$this->assertDatabaseHas(ProductPriceImport::tableName(), [
			'product_variant_id' => $priceModel->product_variant_id,
			'price' => $priceModel->price,
			'price_with_discount' => $priceModel->price_with_discount,
			'discount' => $priceModel->discount,
			'currency' => $priceModel->currency,
			'price_rrp' => $priceModel->price_rrp,
			'price_rrp_with_discount' => $priceModel->price_rrp_with_discount,
			'discount_rrp' => $priceModel->discount_rrp,
			'currency_rrp' => $priceModel->currency_rrp,
		]);
	}
}