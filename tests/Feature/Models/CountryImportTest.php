<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\CountryResource;
use Madwave\Import\Models\CountryImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class CountryImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$country = CountryImport::factory()->make(['id' => 1]);

		$resources = CountryResource::collection([$country])->resolve();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new CountryImport(), $resources);

		$this->assertDatabaseHas(CountryImport::tableName(), [
			'name' => $country->name,
		]);
	}
}