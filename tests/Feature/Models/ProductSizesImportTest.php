<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\SizeResource;
use Madwave\Import\Models\ProductSizesImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class ProductSizesImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {

		$size = ProductSizesImport::factory()->make(['id' => 1]);

		$resources = SizeResource::collection([$size])
			->collection
			->map(fn($sizeResource) => (object) $sizeResource->resolve())
			->each(fn($sizeObject) => $sizeObject->details = [])
			->toArray();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new ProductSizesImport(), $resources);

		$this->assertDatabaseHas(ProductSizesImport::tableName(), [
			'name' => $size->name,
			'category_id' => $size->category_id,
		]);
	}
}