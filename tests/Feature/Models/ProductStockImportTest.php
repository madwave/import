<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\StockResource;
use Madwave\Import\Models\ProductStockImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class ProductStockImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$stock = ProductStockImport::factory()->make(['id' => 1]);

		$resources = StockResource::collection([$stock])->resolve();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new ProductStockImport(), $resources);

		$this->assertDatabaseHas(ProductStockImport::tableName(), [
			'product_variant_id' => $stock->product_variant_id,
			'quantity' => $stock->quantity,
		]);
	}
}