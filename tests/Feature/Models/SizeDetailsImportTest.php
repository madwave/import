<?php

namespace Madwave\Import\Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Madwave\Import\App\Http\Resources\FakeApi\SizeDetailResource;
use Madwave\Import\Models\SizeDetailImport;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Tests\TestCase;

/**
 * @group import
 */
class SizeDetailsImportTest extends TestCase {

	use RefreshDatabase;

	public function test_import() {
		$detail = SizeDetailImport::factory()->make(['id' => 1]);

		$resources = SizeDetailResource::collection([$detail])->resolve();

		$import = new Import(ImportTypeEnum::ALL);
		$import->insertModel(new SizeDetailImport(), $resources);

		$this->assertDatabaseHas(SizeDetailImport::tableName(), [
			'name' => $detail->name,
		]);
	}
}