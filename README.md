**To run the package, you need:**

1) Download the composer package by running the command


    composer install madwave/import
2) Perform database migrations for import by executing the command in the root of the project


    php artisan migrate
3) Next, you need to initialize the import package by running the command below, where `locale` is the localization of the project. To access the download of a specific localization, you must have an API token and an Url address to connect to a remote resource. After executing the command, all the necessary model files will be generated in the `App/Models/Import` directory, as well as the configuration files added `config/madwave.php`


    php artisan madwave:init {locale}
4) Add to env file of your project `API_B2B_TOKEN` and` API_B2B_URL` given to you by Madwave's manager.
5) Execute data import command


    php artisan madwave:import insert
6) To run the import, configure the cron in your application kernel `App\Console\Kernel`

    
    $schedule->command(MadwaveImport::class, ['update'])->everyMinute();

    