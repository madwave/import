<?php

namespace Madwave\Import\Exceptions;

use Madwave\Import\ApiContext;
use Exception;

/**
 * Выбрасывается при ошиках при обращении к API сервисам
 */
class ApiException extends Exception
{
    private array $requestInfo;
    private array $resources = [];

    /**
     * @param ApiContext $context
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(ApiContext $context, string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        $this->requestInfo = $context->toArray();

        parent::__construct($message, $code, $previous);
    }

    /**
     * @param $resources
     * @return void
     */
    public function setResources($resources): void
    {
        $this->resources = $resources;
    }

    /**
     * @return array
     */
    public function getResources(): array
    {
        return $this->resources;
    }

    /**
     * @return array
     */
    public function getRequestInfo(): array
    {
        return $this->requestInfo;
    }
}
