<?php

namespace Madwave\Import;

use Illuminate\Support\Facades\Http;
use Madwave\Import\Exceptions\ApiException;
use Illuminate\Support\Facades\Log;
use stdClass;

/**
 * Сервис для работы с Api
 */
class RequestClient
{
    private ApiContext $context;

    public const REQUEST_ERROR_CODE = 1;
    public const RESPONSE_ERROR_CODE = 2;

    /**
     * @param ApiContext $context
     */
    public function __construct(ApiContext $context)
    {
        $this->context = $context;
    }

    /**
     * @return mixed|void
     * @throws ApiException
     */
    private function execute()
    {
        try {
            $fullUrl = $this->context->getFullUrl() . $this->getParamsAsQueryString();

            $response = $this->sendRequest($fullUrl, $this->context->getMethod());

            if (!empty($response->message)) {
                throw new \Exception($response->message);
            }

            return $response;

        } catch (\Throwable $error) {
            throw new ApiException($this->context, $error->getMessage(), self::REQUEST_ERROR_CODE);

        } catch (ApiException $exception) {
            Log::info($this->context->getFullUrl() . ' - ' . $exception->getMessage());

            throw $exception;
        }
    }

    /**
     * @param string $url
     * @return mixed
     */
    private function sendRequest(string $url, string $method): mixed
    {
        return Http::withToken($this->context->getToken())
            ->withoutVerifying()
            ->$method($url, $this->context->getParams())
            ->object();
    }

    /**
     * @return string
     */
    private function getParamsAsQueryString(): string
    {
        $params = $this->context->getMethod() === 'get' ? $this->context->getParams() : [];
        $params['api_token'] = $this->context->getToken();

        return '?' . http_build_query($params);
    }

    /**
     * @param int $page
     * @param $meta
     * @return array|null
     * @throws ApiException
     */
    public function getResources(int $page, &$meta = null): array|null
    {
        $this->context->setParam('page', $page);
        $resources = $this->execute();

        if ($resources && property_exists($resources, 'meta')) {
            $meta = $resources->meta;
        }

        if ($resources && property_exists($resources, 'data')) {
            $resources = $resources->data ?: [];
        }

        return $resources;
    }

    /**
     * @param array $additionalResources
     * @param int|null $fromPage
     * @return array
     * @throws ApiException
     */
    public function getAllResources(array $additionalResources = [], int $fromPage = null): array
    {
        try {
            $fromPage = $fromPage ?: 1;
            $resources =  $this->getResources($fromPage, $meta) ?? [];
            $allResources = array_merge($additionalResources, $resources);

            if ($meta && $meta->last_page > 1) {
                $countPages = $meta->last_page;

                for ($i = $fromPage + 1; $i <= $countPages; $i++) {
                    $resources =  $this->getResources($i) ?? [];
                    array_push($allResources, ...$resources);
                }
            }
        } catch (ApiException $exception) {
            $exception->setResources($allResources);

            throw $exception;
        }

        return $allResources;
    }
}
