<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Madwave\Import\Factories\GenderFactory;

/**
 * Модель половой принадлежности
 * @property int $id ИД
 * @property string $name Название
 * @property string $updated_at Дата изменения
 */
class GenderImport extends ImportModel {

    use HasFactory;

    protected $table = 'genders';

    /**
     * @inerhitDoc
     */
    protected $fillable = [
        'id',
        'name'
    ];

    protected static function newFactory() {
        return GenderFactory::new();
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->genders();
    }
}
