<?php

namespace Madwave\Import\Models;

use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Madwave\Import\Factories\ProductSizesSizeDetailsFactory;

/**
 * Размеры продукта
 * @property int $size_detail_id ИД детализации размера
 * @property int $size_id ИД размера товара
 * @property string $value Значение
 * @property string $updated_at Дата изменения
 * @property string $created_at Дата добавления
 * @mixin Builder
 */
class ProductSizesSizeDetailsImport extends ImportModel {

    use HasFactory;

    protected $table = 'product_sizes_size_details';
    public array $idFieldNames = ['size_id', 'size_detail_id'];

    /**
     * @inerhitDoc
     */
    protected $fillable = [
        'value',
        'size_detail_id',
        'size_id',
        'created_at',
        'updated_at'
    ];

    protected static function newFactory() {
        return ProductSizesSizeDetailsFactory::new();
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->sizeSizeDetails();
    }
}
