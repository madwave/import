<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Madwave\Import\Factories\SizeDetailFactory;

/**
 * Размеры продукта
 * @property int $id ИД
 * @property string $name Название
 * @property string $value Значение
 * @property array $product_size_id Путь
 * @property string $updated_at Дата изменения
 * @property string $created_at Дата добавления
 */
class SizeDetailImport extends ImportModel {

    use HasFactory;

    protected $table = 'size_details';

    /**
     * @inerhitDoc
     */
    protected $fillable = [
        'id',
        'name',
    ];

    protected static function newFactory() {
        return SizeDetailFactory::new();
    }

    public function sizes() {
        return $this->belongsToMany(
            ProductSizesImport::class,
            ProductSizesSizeDetailsImport::tableName(),
            'size_detail_id',
            'size_id',
        );
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->sizeDetails();
    }
}
