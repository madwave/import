<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Madwave\Import\Factories\ProductVariantsFactory;

/**
 * Вариации продукта
 * @property int $id - ИД
 * @property int $product_id - ИД Продукта
 * @property int $size_id - ИД Размера
 * @property int $color_id - ИД Цвета
 * @property int $article - Артикль
 * @property int $barcode - Штрих-код
 * @property boolean $discontinued - Включенность
 * @property float $gross_weight - Масса брутто
 * @property float $net_weight - Масса нетто
 * @property float $volume - Объем
 * @property float $length - Длина
 * @property float $width - Ширина
 * @property float $height - Высота
 * @property float $carton_length - Длина коробки
 * @property float $carton_width - Ширина коробки
 * @property float $carton_height - Высота коробки
 * @property string $created_at - Дата создания
 * @property string $updated_at - Дата изменения
 * @property ProductSizesImport $size - Размер (объект)
 * @property ProductColorsImport $color - Цвет (объект)
 */
class ProductVariantsImport extends ImportModel {

    use HasFactory;

    protected $table = 'product_variants';

    /**
     * @inerhitDoc
     */
    protected $fillable = [
        'id',
        'product_id',
        'article',
        'barcode',
        'size_id',
        'color_id',
        'discontinued',
        'gross_weight',
        'net_weight',
        'volume',
        'length',
        'width',
        'height',
        'carton_length',
        'carton_width',
        'carton_height',
        'is_new',
        'updated_at',
    ];

    protected static function newFactory() {
        return ProductVariantsFactory::new();
    }

    /**
     * @return HasOne
     */
    public function color()
    {
        return $this->hasOne(ProductColorsImport::class, 'id', 'color_id');
    }

    /**
     * @return HasOne
     */
    public function size()
    {
        return $this->hasOne(ProductSizesImport::class, 'id', 'size_id');
    }

    public function prices(): HasMany {
        return $this->hasMany(ProductPriceImport::class, 'product_variant_id');
    }

    public function stocks(): HasMany {
        return $this->hasMany(ProductStockImport::class, 'product_variant_id');
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->productVariantsByProductIds($params['productIds']);
    }
}
