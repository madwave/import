<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Madwave\Import\Factories\ProductTypeFactory;

/**
 * Категория продукта
 * @property int $id ИД
 * @property string $name Название
 * @property string $updated_at Дата изменения
 */
class ProductTypeImport extends ImportModel {

    use HasFactory;

    protected $table = 'product_types';

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'id',
        'name'
    ];

    protected static function newFactory() {
        return ProductTypeFactory::new();
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->productTypes();
    }
}
