<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Madwave\Import\Factories\CountryFactory;

/**
 * Модель страны
 * @property int $id ИД
 * @property string $name Название
 */
class CountryImport extends ImportModel {

    use HasFactory;

    protected $table = 'countries';

    /**
     * @inerhitDoc
     */
    protected $fillable = [
        'id',
        'name',
    ];

    protected static function newFactory() {
        return CountryFactory::new();
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->locations();
    }
}
