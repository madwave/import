<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Madwave\Import\Factories\ProductPriceFactory;

/**
 * Модель цены
 * @property float $price - Цена
 * @property int $product_variant_id - ИД варианта продукта
 * @property float $price_with_discount - Цена со скидкой
 * @property float $discount - Скидка
 * @property string $currency - валюта
 * @property float $price_rrp - Цена РРЦ
 * @property float $price_rrp_with_discount - Цена РРЦ со скидкой
 * @property float $discount_rrp - Скидка РРЦ
 * @property string $currency_rrp - Валюта РРЦ
 * @property string $created_at - Дата создания
 * @property string $updated_at - Дата изменения
 * @property ProductVariantsImport $variant - Вариант продукта
 */
class ProductPriceImport extends ImportModel {

    use HasFactory;

    protected $table = 'product_prices';

    public array $idFieldNames = ['product_variant_id'];

    /**
     * @inerhitDoc
     */
    protected $fillable = [
        'id',
        'product_variant_id',
        'price',
        'price_with_discount',
        'discount',
        'currency',
        'price_rrp',
        'price_rrp_with_discount',
        'discount_rrp',
        'currency_rrp'
    ];

    protected static function newFactory() {
        return ProductPriceFactory::new();
    }

    /**
     * @return HasOne
     */
    public function variant()
    {
        return $this->hasOne(ProductVariantsImport::class, 'id', 'product_variant_id');
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->productPrices();
    }
}
