<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Madwave\Import\Services\ImportService\ImportRepository;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Madwave\Import\Factories\ProductFactory;

/**
 * Модель продукта
 * @property int $id - ИД
 * @property string $name - Название
 * @property int $category_id - ИД категории
 * @property int $type_id - ИД типа
 * @property int $country_id - ИД страны
 * @property int $brand_id - ИД бренда
 * @property int $gender_id - ИД пола
 * @property string $certificate_ce - Сертификат CE
 * @property string $certificate_eac - Сертификат EAC
 * @property int $measure_id - ИД измерения
 * @property string $description
 * @property string $compositions
 * @property string $components
 * @property string $features
 * @property string $youtube_links
 * @property string $updated_at - Дата изменения
 * @property ProductCategoryImport $productCategory -Категория продукта
 * @property ProductTypeImport $productType - Тип продукта
 * @property BrandImport $brand - Бренд
 * @property CountryImport $country - Страна
 * @property GenderImport $gender - Пол
 * @property MeasureImport $measure - Мера
 * @property ProductVariantsImport[] $variants - Вариации продуктов
 *
 * @method static ProductFactory factory()
 */
class ProductImport extends ImportModel {

    use HasFactory;
    use SoftDeletes;

    protected $table = 'products';

    /**
     * @inerhitDoc
     */
    protected $fillable = [
        'id',
        'name',
        'category_id',
        'type_id',
        'country_id',
        'brand_id',
        'gender_id',
        'certificate_ce',
        'certificate_eac',
        'measure_id',
        'description',
        'compositions',
        'components',
        'features',
        'youtube_links',
        'updated_at',
        'deleted_at',
    ];

    protected static function newFactory() {
        return ProductFactory::new();
    }

    /**
     * @return HasOne
     */
    public function brand()
    {
        return $this->hasOne(BrandImport::class, 'id', 'brand_id');
    }

    /**
     * @return HasOne
     */
    public function country()
    {
        return $this->hasOne(CountryImport::class, 'id', 'country_id');
    }

    /**
     * @return HasOne
     */
    public function gender()
    {
        return $this->hasOne(GenderImport::class, 'id', 'gender_id');
    }

    /**
     * @return HasOne
     */
    public function measure()
    {
        return $this->hasOne(MeasureImport::class, 'id', 'measure_id');
    }

    /**
     * @return HasOne
     */
    public function productCategory()
    {
        return $this->hasOne(ProductCategoryImport::class, 'id', 'category_id');
    }

    /**
     * @return HasOne
     */
    public function productType()
    {
        return $this->hasOne(ProductTypeImport::class, 'id', 'type_id');
    }

    /**
     * @return HasMany
     */
    public function variants()
    {
        return $this->hasMany(ProductVariantsImport::class, 'product_id', 'id');
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->products();
    }

    /**
     * @param ImportRepository $repository
     * @param ImportTypeEnum $type
     * @param array $resources
     * @return array[]|mixed
     */
    public function insertImportResources(ImportRepository $repository, ImportTypeEnum $type, array $resources): mixed {

        $imported = $repository->importAsArray($resources, $type->withCheckingDelete());

        if ($type === ImportTypeEnum::ALL) {
            return collect($resources)
                ->map(fn($product) => $product->variants)
                ->flatten()
                ->toArray();
        }

        return array_merge($imported['added'], $imported['notAdded']);
    }
}
