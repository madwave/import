<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Madwave\Import\Factories\ProductSizesFactory;
use Madwave\Import\Services\ImportService\ImportRepository;
use Madwave\Import\Services\ImportService\ImportTypeEnum;

/**
 * Размеры продукта
 * @property int $id ИД
 * @property string $name Путь
 * @property string $updated_at Дата изменения
 * @property string $created_at Дата добавления
 * @property SizeDetailImport[] $details Детали размера (объект)
 */
class ProductSizesImport extends ImportModel
{
    use HasFactory;

    protected $table = 'product_sizes';

    /**
     * @inerhitDoc
     */
    protected $fillable = [
        'id',
        'name',
        'category_id'
    ];

    protected static function newFactory()
    {
        return ProductSizesFactory::new();
    }

    public function details()
    {
        return $this->belongsToMany(
            SizeDetailImport::class,
            ProductSizesSizeDetailsImport::tableName(),
            'size_id',
            'size_detail_id',
        );
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(ProductCategoryImport::class, 'category_id');
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->productSizes();
    }

    public function insertImportResources(ImportRepository $repository, ImportTypeEnum $type, array $resources): mixed
    {
        $repository->importAsArray($resources, $type->withCheckingDelete());
        $sizeDetails = [];

        foreach ($resources as $size) {
            foreach ($size->details as $detail) {
                $sizeDetails[] = ['size_detail_id' => $detail->id, 'size_id' => $size->id, 'value' => $detail->value];
            }
        }

        return $sizeDetails;
    }
}
