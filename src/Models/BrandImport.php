<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Madwave\Import\Factories\BrandFactory;

/**
 * Модель Бренда
 * @property int $id ИД
 * @property string $name Название
 * @property string $updated_at Дата изменения
 */
class BrandImport extends ImportModel {

    use HasFactory;

    protected $table = 'brands';

    /**
     * @inerhitDoc
     */
    protected $fillable = [
        'id',
        'name'
    ];

    protected static function newFactory() {
        return BrandFactory::new();
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->brands();
    }
}
