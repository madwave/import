<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Madwave\Import\Factories\ProductStockFactory;

/**
 * Модель остатка
 * @property int $quantity - Количество
 * @property int $product_variant_id - ИД варианта продукта
 */
class ProductStockImport extends ImportModel {

    use HasFactory;

    protected $table = 'product_stocks';
    public array $idFieldNames= ['product_variant_id'];

    /**
     * @inerhitDoc
     */
    protected $fillable = [
        'product_variant_id',
        'quantity',
    ];

    protected static function newFactory() {
        return ProductStockFactory::new();
    }

    /**
     * @return HasOne
     */
    public function variant()
    {
        return $this->hasOne(ProductVariantsImport::class, 'id', 'product_variant_id');
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->productStocks();
    }
}
