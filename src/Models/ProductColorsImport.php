<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Madwave\Import\Factories\ProductColorsFactory;

/**
 * Цвета продукта
 * @property int $id ИД
 * @property string $name Путь
 * @property string $hex Путь
 * @property string $updated_at Дата изменения
 * @property string $created_at Дата добавления
 */
class ProductColorsImport extends ImportModel {

    use HasFactory;

    protected $table = 'product_colors';

    /**
     * @inerhitDoc
     */
    protected $fillable = [
        'id',
        'name',
        'hex'
    ];

    protected static function newFactory() {
        return ProductColorsFactory::new();
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->colors();
    }
}
