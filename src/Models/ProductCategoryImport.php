<?php

namespace Madwave\Import\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Madwave\Import\Services\ImportService\ImportModel;
use Madwave\Import\ApiContext;
use Madwave\Import\Factories\ProductCategoryFactory;

/**
 * Категория продукта
 * @property int $id ИД
 * @property int $parent_id ИД родителя
 * @property string $name Название
 */
class ProductCategoryImport extends ImportModel {

    use HasFactory;

    protected $table = 'product_categories';

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'id',
        'name',
        'parent_id'
    ];

    protected static function newFactory() {
        return ProductCategoryFactory::new();
    }

    /**
     * @param ApiContext $context
     * @param ...$params
     * @return void
     */
    public function fillContext(ApiContext $context, ...$params): void
    {
        $context->productCategories();
    }
}
