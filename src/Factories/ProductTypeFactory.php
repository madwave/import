<?php

namespace Madwave\Import\Factories;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Madwave\Import\Models\ProductTypeImport;

class ProductTypeFactory extends Factory {

	protected $model = ProductTypeImport::class;

	public function definition() {
		return [
			'name' => $this->faker->name(),
			'updated_at' => new Carbon($this->faker->date),
		];
	}
}