<?php

namespace Madwave\Import\Factories;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Madwave\Import\Models\ProductCategoryImport;
use Madwave\Import\Models\ProductSizesImport;

class ProductSizesFactory extends Factory {

	protected $model = ProductSizesImport::class;

	public function definition() {
		return [
			'name' => $this->faker->name(),
			'category_id' => ProductCategoryImport::factory(),
			'updated_at' => new Carbon($this->faker->date),
		];
	}
}