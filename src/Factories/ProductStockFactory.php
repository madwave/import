<?php

namespace Madwave\Import\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Madwave\Import\Models\ProductStockImport;
use Madwave\Import\Models\ProductVariantsImport;

class ProductStockFactory extends Factory {

	protected $model = ProductStockImport::class;

	public function definition() {
		return [
			'product_variant_id' => ProductVariantsImport::factory(),
			'quantity' => $this->faker->randomNumber(2)
		];
	}
}