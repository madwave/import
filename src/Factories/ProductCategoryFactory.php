<?php

namespace Madwave\Import\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Madwave\Import\Models\ProductCategoryImport;

class ProductCategoryFactory extends Factory {

	protected $model = ProductCategoryImport::class;

	public function definition() {
		return [
			'name' => $this->faker->word(),
			'parent_id' => null,
		];
	}

	public function withParent() {
		return $this->state(function (array $attributes) {
			return [
				'parent_id' => ProductCategoryImport::factory(),
			];
		});
	}
}