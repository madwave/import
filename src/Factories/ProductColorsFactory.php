<?php

namespace Madwave\Import\Factories;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Madwave\Import\Models\ProductColorsImport;

class ProductColorsFactory extends Factory {

	protected $model = ProductColorsImport::class;

	public function definition() {
		return [
			'name' => $this->faker->colorName(),
			'hex' => $this->faker->hexColor(),
			'updated_at' => new Carbon($this->faker->date),
		];
	}
}