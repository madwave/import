<?php

namespace Madwave\Import\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Madwave\Import\Models\CountryImport;

class CountryFactory extends Factory {

	protected $model = CountryImport::class;

	public function definition() {
		return [
			'name' => $this->faker->country(),
		];
	}
}