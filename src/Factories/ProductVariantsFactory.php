<?php

namespace Madwave\Import\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use Madwave\Import\Models\ProductColorsImport;
use Madwave\Import\Models\ProductImport;
use Madwave\Import\Models\ProductSizesImport;
use Madwave\Import\Models\ProductVariantsImport;

class ProductVariantsFactory extends Factory {

	protected $model = ProductVariantsImport::class;

	public function definition() {
		return [
			'product_id' => ProductImport::factory(),
			'size_id' => ProductSizesImport::factory(),
			'color_id' => ProductColorsImport::factory(),
			'article' => 'M' . $this->faker->randomNumber(9) . 'W',
			'barcode' => $this->faker->randomNumber(9),
			'discontinued' => $this->faker->boolean,
			'gross_weight' => $this->faker->randomNumber(2),
			'net_weight' => $this->faker->randomNumber(2),
			'volume' => $this->faker->randomNumber(3),
			'length' => $this->faker->randomNumber(2),
			'width' => $this->faker->randomNumber(2),
			'height' => $this->faker->randomNumber(2),
			'carton_length' => $this->faker->randomNumber(2),
			'carton_width' => $this->faker->randomNumber(2),
			'carton_height' => $this->faker->randomNumber(2),
			'is_new' => $this->faker->boolean,
			'updated_at' => new Carbon($this->faker->date),
		];
	}
}