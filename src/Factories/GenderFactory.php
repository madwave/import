<?php

namespace Madwave\Import\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use Madwave\Import\Models\GenderImport;

class GenderFactory extends Factory {

	protected $model = GenderImport::class;

	public function definition() {
		return [
			'name' => $this->faker->name(),
			'updated_at' => new Carbon($this->faker->date),
		];
	}
}