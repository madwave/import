<?php

namespace Madwave\Import\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Madwave\Import\Models\ProductSizesSizeDetailsImport;
use Madwave\Import\Models\ProductSizesImport;
use Madwave\Import\Models\SizeDetailImport;

class ProductSizesSizeDetailsFactory extends Factory {

	protected $model = ProductSizesSizeDetailsImport::class;

	public function definition() {
		return [
			'value' => $this->faker->numberBetween(20, 100) . '-' . $this->faker->numberBetween(20, 100),
			'size_id' => ProductSizesImport::factory(),
			'size_detail_id' => SizeDetailImport::factory(),
		];
	}
}