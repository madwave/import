<?php

namespace Madwave\Import\Factories;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Madwave\Import\Models\MeasureImport;

class MeasureFactory extends Factory {

	protected $model = MeasureImport::class;

	public function definition() {
		return [
			'name' => $this->faker->word(),
			'updated_at' => new Carbon($this->faker->date),
		];
	}
}