<?php

namespace Madwave\Import\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Madwave\Import\Models\BrandImport;

class BrandFactory extends Factory {

	protected $model = BrandImport::class;

	public function definition() {
		return [
			'name' => $this->faker->name(),
			'updated_at' => $this->faker->date(),
			'created_at' => $this->faker->date()
		];
	}
}