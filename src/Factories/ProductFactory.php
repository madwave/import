<?php

namespace Madwave\Import\Factories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Madwave\Import\Models\BrandImport;
use Madwave\Import\Models\CountryImport;
use Madwave\Import\Models\GenderImport;
use Madwave\Import\Models\MeasureImport;
use Madwave\Import\Models\ProductCategoryImport;
use Madwave\Import\Models\ProductImport;
use Madwave\Import\Models\ProductTypeImport;

/**
 * @method ProductImport make($attributes = [], ?Model $parent = null)
 */
class ProductFactory extends Factory {

	protected $model = ProductImport::class;

	public function definition() {
		return [
			'name' => $this->faker->name(),
			'certificate_ce' => $this->faker->randomNumber(),
			'certificate_eac' => $this->faker->randomNumber(),
			'measure_id' => MeasureImport::factory(),
			'description' => $this->faker->text(1000),
			'compositions' => $this->faker->text(500),
			'components' => $this->faker->text(500),
			'features' => $this->faker->text(500),
			'youtube_links' => $this->faker->url(),
			'updated_at' => new Carbon($this->faker->date),

			'category_id' => ProductCategoryImport::factory(),
			'type_id' => ProductTypeImport::factory(),
			'country_id' => CountryImport::factory(),
			'brand_id' => BrandImport::factory(),
			'gender_id' => GenderImport::factory(),
		];
	}
}