<?php

namespace Madwave\Import\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Madwave\Import\Models\ProductPriceImport;
use Madwave\Import\Models\ProductVariantsImport;

class ProductPriceFactory extends Factory {

	protected $model = ProductPriceImport::class;

	private function applyDiscount(float $price, int $discount): float {
		$newPrice = $price - $price * ($discount / 100);
		return round($newPrice, 2);
	}

	public function definition() {

		$price = $this->faker->randomFloat(2, 10, 50);
		$discount = rand(0, 1) ? $this->faker->numberBetween(20, 50) : 0;
		$currency = $this->faker->currencyCode();
		$rrpCoff = 1.05;

		return [
			'product_variant_id' => ProductVariantsImport::factory(),
			'price' => $price,
			'price_with_discount' => $this->applyDiscount($price, $discount),
			'discount' => $discount,
			'currency' => $currency,
			'price_rrp' => round($price * $rrpCoff),
			'price_rrp_with_discount' => round($this->applyDiscount($price * $rrpCoff, $discount)),
			'discount_rrp' => $discount,
			'currency_rrp' => $currency,
		];
	}
}