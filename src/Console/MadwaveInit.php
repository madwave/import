<?php

namespace Madwave\Import\Console;

use Illuminate\Console\Command;

/**
 * Инициализация пакета Madwave import
 */
class MadwaveInit extends Command
{

    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'madwave:init {locale=ru}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Create Models for import';

    /**
     * @return void
     */
    public function handle()
    {
        file_put_contents(
            $this->laravel->basePath() . '/config/import.php',
            file_get_contents(__DIR__ . '/../stubs/config/' . $this->argument('locale') . '/madwave.stub')
        );

        $this->info('init success');
    }
}
