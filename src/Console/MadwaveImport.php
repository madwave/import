<?php

namespace Madwave\Import\Console;

use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Madwave\Import\Services\ImportService\Import;
use Madwave\Import\Exceptions\ApiException;
use Illuminate\Console\Command;
use Exception;

/**
 * Импорт данных Madwave
 */
class MadwaveImport extends Command
{

    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'madwave:import {argument=all} {--updatedAt=} {--force} {--manual}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Console commands for working with import';

    /**
     * @return void
     * @throws Exception
     */
    public function handle(): void
    {
        switch ($this->argument('argument')) {
            case 'all':
                $this->runImport(new Import(ImportTypeEnum::ALL));
                break;
            case 'only-update':
                $import = new Import(ImportTypeEnum::ONLY_UPDATED, $this->option('updatedAt'));
                $this->runImport($import);
                break;
        }
    }

    /**
     * @param Import $import
     * @return void
     * @throws ApiException
     */
    protected function runImport(Import $import): void
    {
        if ( $this->option('manual')) {
            $this->runManualImport($import);
        } else {
            $this->runQueuedImport($import);
        }
    }

    /**
     * @param Import $import
     * @return void
     * @throws ApiException
     */
    protected function runManualImport(Import $import): void
    {
        $this->info(sprintf("Running import of type: %s", $import->type->value));

        $ok = $import->setCallback(fn($className) => $this->comment($className . ' - success'))->run($this->option('force'));

        if ($ok) {
            $this->info('Import successful');
        } else {
            $this->error('Import canceled');
            $this->comment('Try to use --force');
        }
    }

    /**
     * @param Import $import
     * @return void
     * @throws ApiException
     */
    protected function runQueuedImport(Import $import): void
    {
        $import->onQueue(config('import.types.' . $import->type->value . '.queue'))->run($this->option('force'));
    }
}
