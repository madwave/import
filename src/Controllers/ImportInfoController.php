<?php

namespace Madwave\Import\Controllers;

use Madwave\Import\Services\ImportService\DataImportInfo\DataImportStorage;
use Madwave\Import\Services\ImportService\DataImportInfo\HistoryImportInfoResource;
use Madwave\Import\Services\ImportService\DataImportInfo\LastImportInfoResource;
use Madwave\Import\Services\ImportService\ImportTypeEnum;
class ImportInfoController
{
    public function new()
    {
        $importStorage = new DataImportStorage(ImportTypeEnum::ONLY_UPDATED);
        $lastImportInfo = $importStorage->getResource(LastImportInfoResource::class);
        $importInfoHistory = $importStorage->getResource(HistoryImportInfoResource::class);

        return view('import::import-info.new', compact('importInfoHistory', 'lastImportInfo'));
    }

    public function all()
    {
        $importStorage = new DataImportStorage(ImportTypeEnum::ALL);
        $lastImportInfo = $importStorage->getResource(LastImportInfoResource::class);
        $importInfoHistory = $importStorage->getResource(HistoryImportInfoResource::class);

        return view('import::import-info.all', compact('importInfoHistory', 'lastImportInfo'));
    }
}
