<?php

namespace Madwave\Import\Controllers;

use Illuminate\Support\Facades\Artisan;
class ImportController
{
    public function tryNew()
    {
        Artisan::call('madwave:import update');

        return redirect(route('import.info.new'));
    }

    public function tryAll()
    {
        Artisan::call('madwave:import --force');

        return redirect(route('import.info.all'));
    }
}
