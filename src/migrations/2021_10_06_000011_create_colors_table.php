<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColorsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('product_colors', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ИД');
            $table->string('name' , 255)->nullable()->comment('Название');
            $table->string('hex' , 255)->nullable()->comment('Шестнадцатеричный');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `product_colors` comment 'Цвета продуктов'");
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_colors');
    }
}
