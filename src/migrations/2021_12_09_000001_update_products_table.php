<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProductsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->text('description')->after('name')->comment('Описание')->nullable();
            $table->text('compositions')->after('description')->comment('Материалы')->nullable();
            $table->text('components')->after('measure_id')->comment('Компоненты')->nullable();
            $table->text('features')->after('components')->comment('Особенности')->nullable();
            $table->text('youtube_links')->after('features')->comment('Ссылка на ютуб')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('compositions');
            $table->dropColumn('components');
            $table->dropColumn('features');
            $table->dropColumn('youtube_links');
        });
    }
}
