<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductStocksTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('product_stocks', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ИД');
            $table->bigInteger('product_variant_id')->unsigned()->comment('ИД варианта продукта');
            $table->bigInteger('quantity')->nullable()->comment('Количество');
            $table->timestamps();
            $table->foreign('product_variant_id')
                ->references('id')
                ->on('product_variants')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
        DB::statement("ALTER TABLE `product_stocks` comment 'Продуктовые остатки'");
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_stocks');
    }
}
