<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProductSizeProductSizeDetailTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('product_sizes_size_details', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ИД');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('product_sizes_size_details', ['id']);
    }
}
