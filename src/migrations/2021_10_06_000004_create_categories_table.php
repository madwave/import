<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ИД');
            $table->string('name', 255)->comment('Название');
            $table->integer('parent_id')->nullable(true)->comment('ИД родителя');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `product_categories` comment 'Категории продуктов'");
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_categories');
    }
}
