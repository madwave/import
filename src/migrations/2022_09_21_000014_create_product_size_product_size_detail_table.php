<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSizeProductSizeDetailTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('size_details', function (Blueprint $table) {
            $table->dropColumn('product_size_id');
            $table->dropColumn('value');
        });

        Schema::create('product_sizes_size_details', function (Blueprint $table) {
            $table->foreignId('size_id')->index();
            $table->foreignId('size_detail_id')->index();
            $table->string('value')->nullable();
//            $table->foreign('size_id')
//                ->references('id')
//                ->on('product_sizes')
//                ->cascadeOnUpdate()
//                ->cascadeOnDelete();
//            $table->foreign('size_detail_id')
//                ->references('id')
//                ->on('size_details')
//                ->cascadeOnUpdate()
//                ->cascadeOnDelete();
            $table->timestamps();

            $table->unique(['size_id', 'size_detail_id']);
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sizes_size_details');
        Schema::table('size_details', function (Blueprint $table) {
            $table->bigInteger('product_size_id')->unsigned()->comment('ИД размера продукта');
            $table->string('value')->nullable();
        });
    }
}
