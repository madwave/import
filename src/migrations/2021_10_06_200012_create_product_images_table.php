<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductImagesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('product_images', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ИД');
            $table->string('path', 255)->nullable()->comment('Путь к файлу');
            $table->bigInteger('variant_id')->unsigned()->nullable()->comment('ИД вариации продукта');
            $table->timestamps();
            $table->foreign('variant_id')
                ->references('id')
                ->on('product_variants')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
        DB::statement("ALTER TABLE `product_images` comment 'Изображения для продуктов'");
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_images');
    }
}
