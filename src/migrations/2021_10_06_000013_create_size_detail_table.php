<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSizeDetailTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('size_details', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ИД');
            $table->string('name', 255)->nullable()->comment('Название');
            $table->string('value', 255)->nullable()->comment('Значение');
            $table->bigInteger('product_size_id')->unsigned()->comment('ИД размера продукта');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `size_details` comment 'Детально о размере продукта'");
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('size_details');
    }
}
