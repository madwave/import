<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ИД');
            $table->string('name', 255)->nullable()->comment('Название');
            $table->bigInteger('category_id')->nullable()->unsigned()->comment('ИД категории');
            $table->bigInteger('type_id')->nullable()->unsigned()->comment('ИД типа продукта');
            $table->bigInteger('country_id')->nullable()->unsigned()->comment('ИД страны');
            $table->bigInteger('brand_id')->nullable()->unsigned()->comment('ИД бренда');
            $table->bigInteger('gender_id')->nullable()->unsigned()->comment('ИД половой принадлежности');
            $table->bigInteger('measure_id')->nullable()->unsigned()->comment('ИД меры измерения');
            $table->string('certificate_ce')->comment('ИД сертификата CE');
            $table->string('certificate_eac')->comment('ИД сертификата EAC');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('category_id')
                ->references('id')
                ->on('product_categories')
                ->cascadeOnUpdate()
                ->nullOnDelete();
            $table->foreign('type_id')
                ->references('id')
                ->on('product_types')
                ->cascadeOnUpdate()
                ->nullOnDelete();
            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->cascadeOnUpdate()
                ->nullOnDelete();
            $table->foreign('brand_id')
                ->references('id')
                ->on('brands')
                ->cascadeOnUpdate()
                ->nullOnDelete();
            $table->foreign('gender_id')
                ->references('id')
                ->on('genders')
                ->cascadeOnUpdate()
                ->nullOnDelete();
            $table->foreign('measure_id')
                ->references('id')
                ->on('measures')
                ->cascadeOnUpdate()
                ->nullOnDelete();
        });
        DB::statement("ALTER TABLE `products` comment 'Продукты'");
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
