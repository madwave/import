<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ИД');
            $table->bigInteger('product_variant_id')->unsigned()->comment('ИД варианта продукта');
            $table->float('price')->nullable()->comment('Цена');
            $table->float('price_with_discount')->nullable()->comment('Цена со скидкой');
            $table->float('discount')->nullable()->comment('Скидка');
            $table->string('currency', 10)->nullable()->comment('Валюта');
            $table->bigInteger('price_rrp')->nullable()->comment('Рекомендованная розничная цена');
            $table->bigInteger('price_rrp_with_discount')->nullable()->comment('Рекомендованная розничная цена со скидкой');
            $table->float('discount_rrp')->nullable()->comment('Скидка для рекомендованной розничной цены');
            $table->string('currency_rrp', 10)->nullable()->comment('Валюта РРЦ');
            $table->timestamps();
            $table->foreign('product_variant_id')
                ->references('id')
                ->on('product_variants')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
        DB::statement("ALTER TABLE `product_prices` comment 'Продуктовые цены'");
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
    }
}
