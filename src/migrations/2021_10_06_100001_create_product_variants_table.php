<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVariantsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('product_variants', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('ИД');
            $table->string('article')->comment('Артикль');
            $table->string('barcode')->nullable()->comment('Штрих-код');
            $table->bigInteger('product_id')->unsigned()->comment('ИД продукта');
            $table->bigInteger('size_id')->unsigned()->comment('ИД размера');
            $table->bigInteger('color_id')->nullable()->unsigned()->comment('ИД цвета');
            $table->string('discontinued')->comment('Законченный');
            $table->float('gross_weight')->comment('Вес брутто');
            $table->float('net_weight')->comment('Вес нетто');
            $table->float('volume')->nullable()->comment('Объем');
            $table->float('length')->nullable()->comment('Длина');
            $table->float('width')->nullable()->comment('Ширина');
            $table->float('height')->nullable()->comment('Высота');
            $table->float('carton_length')->nullable()->comment('Длина коробки');
            $table->float('carton_width')->nullable()->comment('Ширина коробки');
            $table->float('carton_height')->nullable()->comment('Высота коробки');
            $table->timestamps();
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('size_id')
                ->references('id')
                ->on('product_sizes')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('color_id')
                ->references('id')
                ->on('product_colors')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
        DB::statement("ALTER TABLE `product_variants` comment 'Вариации продуктов'");
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variants');
    }
}
