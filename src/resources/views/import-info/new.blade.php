<?php
/**
 * @var \Madwave\Import\Services\ImportService\DataImportInfo\LastImportInfoResource $lastImportInfo
 * @var \Madwave\Import\Services\ImportService\DataImportInfo\HistoryImportInfoResource $importInfoHistory
 */
?>

<h2>Last import info</h2>
<div style="margin-bottom: 30px; font-size: 16px">
    <b>
        Import at - {{ $lastImportInfo->import_at }}
    </b>
</div>
<div style=" margin-bottom: 30px">
    @foreach($lastImportInfo->stages as $stageName => $stage)
        <div style="width: 50%;">
            <div style="border: 1px solid black; padding: 5px; width: auto; display: flex; justify-content: space-between; color: {{ $stage ?'green':'red' }}">
                <div>
                    <b>
                        {{ str_replace('Madwave\\Import\\Models\\', '', $stageName) }}
                    </b>
                </div>
                <div style=" text-align: right">
                    last time was performed:
                    <b>
                        {{ $stage ?: 'cancelled or performing' }}
                    </b>
                </div>
            </div>
        </div>
    @endforeach
</div>
<form action="{{ route('import.tryNew') }}" method="post">
    @csrf
    <button>Try new import</button>
</form>