<?php

namespace Madwave\Import;

use Illuminate\Support\Carbon;
use Exception;

/**
 * Класс для формирования данных запроса к апи B2B
 */
class ApiContext
{
    private $url;
    private $token;
    private $params = [];
    private $method = 'get';
    private $urn = '';

    public function __construct()
    {
        if (empty(env('API_B2B_TOKEN'))) {
            throw new Exception('API token do not find, add "API_B2B_TOKEN" variable in env file');
        }
        if (empty(env('API_B2B_URL'))) {
            throw new Exception('API url do not find, add "API_B2B_URL" variable in env file');
        }
        $this->url = env('API_B2B_URL');
        $this->token = env('API_B2B_TOKEN');
    }

    /**
     * Получение всех брендов
     * @return void
     */
    public function brands(): void
    {
        $this->urn = '/api/brands';
    }

    /**
     * Получение категорий продуктов
     * @return void
     */
    public function productCategories(): void
    {
        $this->urn = '/api/categories';
        $this->setParam('locale', config('import.locale'));
        $this->setParam('warehouse_id', config('import.warehouse_id'));
    }

    /**
     * Получение всех локаций
     * @return void
     */
    public function locations(): void
    {
        $this->urn = '/api/countries';
        $this->setParam('locale', config('import.locale'));
    }

    /**
     * Получение полов
     * @return void
     */
    public function genders(): void
    {
        $this->urn = '/api/products/properties/genders';
        $this->setParam('locale', config('import.locale'));
    }

    /**
     * Получение всех цветов
     * @return void
     */
    public function colors(): void
    {
        $this->urn = '/api/products/properties/colors';
        $this->setParam('locale', config('import.locale'));
    }

    /**
     * Получение меры измерения
     * @return void
     */
    public function measures(): void
    {
        $this->urn = '/api/products/properties/measures';
        $this->setParam('locale', config('import.locale'));
    }

    /**
     * Получение продуктовых размеров
     * @return void
     */
    public function productSizes(): void
    {
        $this->urn = '/api/products/properties/sizes';
        $this->setParam('locale', config('import.locale'));
    }

    /**
     * Получение отношения размеров продуктов к их детализации
     * @return void
     */
    public function sizeSizeDetails(): void
    {
        $this->urn = '/api/products/properties/size-details';
        $this->setParam('locale', config('import.locale'));
    }

    /**
     * Получить детали размера по id размера
     * @return void
     */
    public function sizeDetails(): void
    {
        $this->urn = '/api/products/properties/size-details';
        $this->setParam('locale', config('import.locale'));
    }

    /**
     * Получить детали размера по id размера
     * @return void
     */
    public function productSizeDetails(): void
    {
        $this->urn = '/api/products/properties/size-details';
        $this->setParam('locale', config('import.locale'));
    }

    /**
     * Получение типы продуктов
     * @return void
     */
    public function productTypes(): void
    {
        $this->urn = '/api/products/properties/types';
        $this->setParam('locale', config('import.locale'));
    }

    /**
     * Создание заказа
     * @return void
     */
    public function createOrder(): void
    {
        $this->urn = '/api/orders/create';
        $this->method = 'put';
    }

    /**
     * Получение сертификатов продукта
     * @param $productId
     * @return void
     */
    public function certificates($productId): void
    {
        $this->urn = '/api/products/' . $productId . '/certificates';
    }

    /**
     * Получение всех продуктов
     * @param $category
     * @return void
     */
    public function products($category = null): void
    {
        $this->urn = '/api/products';
        $this->setParam('locale', config('import.locale'));
        $this->setParam('warehouse_id', config('import.warehouse_id'));

        if ($category) {
            $this->setParam('category', $category);
        }
    }

    /**
     * Получение одного продукта
     * @param $productId
     * @return void
     */
    public function product($productId): void
    {
        $this->urn = '/api/products/' . $productId;
        $this->setParam('locale', config('import.locale'));
    }

    /**
     * Получить вариации продуктов по productId
     * @param array $productIds массив product_id
     * @return void
     */
    public function productVariantsByProductIds(array $productIds): void
    {
        $this->method = 'post';
        $this->urn = '/api/products/variants';
        $this->setParam('locale', config('import.locale'));
        $this->setParam('warehouse_id', config('import.warehouse_id'));

        $formedIds = [];
        foreach ($productIds as $id) {
            $formedIds[] = $id['id'];
        }

        $this->setParam('ids', $formedIds);
    }

    /**
     * Получение вариаций продукта
     * @param int $productId
     * @return void
     */
    public function productVariant($productId): void
    {
        $this->urn = '/api/products/variants/' . $productId;
        $this->setParam('locale', config('import.locale'));
        $this->setParam('warehouse_id', config('import.warehouse_id'));
    }

    /**
     * Получение складов
     * @return void
     */
    public function warehouses(): void
    {
        $this->urn = '/api/products/warehouses';
    }

    /**
     * Получение запасов
     * @return void
     */
    public function productStocks(): void
    {
        $this->urn = '/api/products/stock';
        $this->method = 'post';
        $this->setParam('warehouse_id', config('import.warehouse_id'));
    }

    /**
     * Получение всех цен
     * @return void
     */
    public function productPrices(): void
    {
        $this->urn = '/api/products/stock/price';
        $this->method = 'get';
        $this->setParam('warehouse_id', config('import.warehouse_id'));
    }

    /**
     * @param ?string $date example: '2014-03-30 03:00:00'
     * @throws Exception
     */
    public function setUpdatedAtUnix(string $date = null): void
    {
        $this->setParam('updated_at', $date ? Carbon::parse($date)->getTimestamp() : null);
    }

    /**
     * @param string $key
     * @param mixed|null $value
     * @return void
     */
    public function setParam(string $key, mixed $value = null): void
    {
        $this->params[$key] = $value;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return string
     */
    public function getFullUrl(): string
    {
        return $this->url . $this->urn;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return mixed
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'params' => $this->getParams(),
            'url' => $this->getFullUrl(),
            'method' => $this->method,
        ];
    }
}
