<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\ProductStockImport;

class StockResource extends JsonResource {

	const IMPORT_MODEL = ProductStockImport::class;

	public function toArray($request) {
		return [
			"product_variant_id" => $this->variant->id,
			"article" => $this->variant->article,
			"quantity" => $this->quantity,
		];
	}
}
