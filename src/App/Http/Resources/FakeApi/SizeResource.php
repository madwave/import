<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\ProductSizesImport;

class SizeResource extends JsonResource {

	const IMPORT_MODEL = ProductSizesImport::class;

	public function toArray($request) {
		$sizesDetails = $this->details()->withPivot('value')->get();
		return [
			'id' => $this->id,
			'updated_at' => $this->updated_at->toDateTimeString(),
			'name' => $this->name,
			'category_name' => $this->category->name,
			'category_id' => $this->category_id,
			'weight' => 123, // не используется при импорте
			'details' => SizesSizeDetailResource::collection($sizesDetails->pluck('pivot')),
		];
	}
}
