<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\ProductPriceImport;

class PriceResource extends JsonResource {

	const IMPORT_MODEL = ProductPriceImport::class;

	public function toArray($request) {
		return [
			'product_variant_id' => $this->variant->id,
			'article' => $this->variant->article,
			'price' => $this->price,
			'price_with_discount' => $this->price_with_discount,
			'discount' => $this->discount,
			'currency' => $this->currency,
			'price_rrp' => $this->price_rrp,
			'price_rrp_with_discount' => $this->price_rrp_with_discount,
			'discount_rrp' => $this->discount_rrp,
			'currency_rrp' => $this->currency_rrp,
		];
	}
}
