<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\ProductTypeImport;

class TypeResource extends JsonResource {

	const IMPORT_MODEL = ProductTypeImport::class;

	public function toArray($request) {
		return [
			'id' => $this->id,
			'updated_at' => $this->updated_at->toDateTimeString(),
			'name' => $this->name,
		];
	}
}
