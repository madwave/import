<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\ProductPriceImport;
use Madwave\Import\Models\ProductStockImport;
use Madwave\Import\Models\ProductVariantsImport;

class VariantResource extends JsonResource {

	const IMPORT_MODEL = ProductVariantsImport::class;

	public function toArray($request) {
		$price = $this->prices->first();
		$stock = $this->stocks->first();
		return [
			'id' => $this->id,
			'product_id' => $this->product_id,
			'article' => $this->article,
			'barcode' => $this->when(!$request->isMethod('post'), $this->barcode), // TODO нет в другом запросе
			'size_id' => $this->size_id,
			'size' => $this->size->name,
			'color_id' => $this->color_id,
			'color' => $this->color->name,
			'discontinued' => $this->discontinued,
			'itf14' => 'itf14Value', // не используется при импорте
			'gross_weight' => $this->gross_weight,
			'net_weight' => $this->net_weight,
			'volume' => $this->volume,
			'length' => $this->length,
			'width' => $this->width,
			'height' => $this->height,
			'carton_length' => $this->carton_length,
			'carton_width' => $this->carton_width,
			'carton_height' => $this->carton_height,
			'outer_carton' => $this->outer_carton,
			'price' => $price->price,
			'price_with_discount' => $price->price_with_discount,
			'discount' => $price->discount,
			'currency' => $price->currency,
			'price_rrp' => $price->price_rrp,
			'price_rrp_with_discount' => $price->price_rrp_with_discount,
			'discount_rrp' => $price->discount_rrp,
			'currency_rrp' => $price->currency_rrp,
			'stock' => $stock ? $stock->quantity : 0,
			'is_new' => $this->is_new,
			'first_stock_received_date' => now()->subMonths(6)->toDateTimeString(),
			'images' => [
				'https://b2b.madwave.eu/img/no-image.jpg',
			],
			'updated_at' => $this->updated_at->toDateTimeString(),
		];


	}
}
