<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\CountryImport;

class CountryResource extends JsonResource {

	const IMPORT_MODEL = CountryImport::class;

	public function toArray($request) {
		return [
			'id' => $this->id,
			'name' => $this->name,
		];
	}
}
