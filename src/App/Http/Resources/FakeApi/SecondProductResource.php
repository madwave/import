<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\ProductImport;

class SecondProductResource extends JsonResource {

	const IMPORT_MODEL = ProductImport::class;

	public function toArray($request) {
		return [
			'id' => $this->id,
			'name' => $this->name,
			'category_id' => $this->category_id,
			'type_id' => $this->type_id,
			'country_id' => $this->country_id,
			'brand_id' => $this->brand_id,
			'compositions' => $this->compositions,
			'components' => $this->components,
			'description' => $this->description,
			'operation_life' => 0, // не используется при импорте
			'features' => $this->features,
			'youtube_links' => $this->youtube_links,
			'gender_id' => $this->gender_id,
			'certificate_ce' => $this->certificate_ce,
			'certificate_eac' => $this->certificate_eac,
			'measure_id' => $this->measure_id,
			'updated_at' => $this->updated_at->toDateTimeString(),
			'variants' => VariantResource::collection($this->variants),
		];
	}
}
