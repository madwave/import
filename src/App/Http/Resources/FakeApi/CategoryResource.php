<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\ProductCategoryImport;

class CategoryResource extends JsonResource {

	const IMPORT_MODEL = ProductCategoryImport::class;

	public function toArray($request) {
		return [
			'id' => $this->id,
			'name' => $this->name,
			'parent_id' => $this->parent_id,
		];
	}
}
