<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\ProductImport;

class ProductResource extends JsonResource {

	const IMPORT_MODEL = ProductImport::class;

	public function toArray($request) {
		return [
			'id' => $this->id,
			'name' => $this->name,
			'category_id' => $this->category_id,
			'type_id' => $this->type_id,
			'country_id' => $this->country_id,
			'brand_id' => $this->brand_id,
			'gender_id' => $this->gender_id,
			'certificate_ce' => $this->certificate_ce,
			'certificate_eac' => $this->certificate_eac,
			'measure_id' => $this->measure_id,
			'updated_at' => $this->updated_at->toDateTimeString(),
			'category' => $this->category->name,
			'brand' => $this->brand->name,
			'gender' => $this->gender->name,
			'compositions' => $this->compositions,
			'components' => $this->components,
			'description' => $this->description,
			'type' => $this->type->name,
			'measure' => $this->measure->name,
			'minipack' => '20', // // не используется при импорте
			'country' => $this->country->name,
			'operation_life' => '0', // не используется при импорте
			'features' => $this->features,
			'promo_files' => '',
			'hs_code' => 111111111, // не используется при импорте
			'tn_ved_code' => 111111111, // не используется при импорте
			'certificates' => '', // не используется при импорте
			'youtube_links' => $this->youtube_links,
		];
	}
}
