<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ImportCollection extends ResourceCollection {

	public function __construct($resource, $collects=null) {
		if ($collects) {
			$this->collects = $collects;
		}

		parent::__construct($resource);
	}

	public function toArray($request) {
		return [
			'data' => $this->collection
		];
	}
}
