<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\ProductSizesSizeDetailsImport;

class SizesSizeDetailResource extends JsonResource {

	const IMPORT_MODEL = ProductSizesSizeDetailsImport::class;

	public function toArray($request) {
		return [
			'id' => $this->size_detail_id,
			'value' => $this->value,
		];
	}
}
