<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\GenderImport;

class GenderResource extends JsonResource {

	const IMPORT_MODEL = GenderImport::class;

	public function toArray($request) {
		return [
			'id' => $this->id,
			'name' => $this->name,
			'updated_at' => $this->updated_at->toDateTimeString(),
		];
	}
}
