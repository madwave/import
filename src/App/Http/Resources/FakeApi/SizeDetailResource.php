<?php

namespace Madwave\Import\App\Http\Resources\FakeApi;

use Illuminate\Http\Resources\Json\JsonResource;
use Madwave\Import\Models\SizeDetailImport;

class SizeDetailResource extends JsonResource {

	const IMPORT_MODEL = SizeDetailImport::class;

	public function toArray($request) {
		return [
			'id' => $this->id,
			'updated_at' => $this->updated_at->toDateTimeString(),
			'name' => $this->name,
		];
	}
}
