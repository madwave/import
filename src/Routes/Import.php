<?php

use Illuminate\Support\Facades\Route;
use Madwave\Import\Controllers\ImportInfoController;
use Madwave\Import\Controllers\ImportController;

Route::group(['prefix' => 'import', 'middleware' => config('import.middleware.import')], function () {
    Route::group(['prefix' => 'info'], function () {
        Route::get('new', [ImportInfoController::class, 'new'])->name('import.info.new');
        Route::get('all', [ImportInfoController::class, 'all'])->name('import.info.all');
    });

    Route::post('try-new', [ImportController::class, 'tryNew'])->name('import.tryNew');
    Route::post('try-all', [ImportController::class, 'tryAll'])->name('import.tryAll');
});