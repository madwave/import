<?php

namespace Madwave\Import\Events;

use App\Events\Import\CreatedModelEventInterface;
use Madwave\Import\Services\ImportService\ImportModel;

class CreatedModelEvent implements CreatedModelEventInterface {

    public ImportModel $model;

    public function __construct(ImportModel $model)
    {
        $this->model = $model;
    }

    public function getModel(): mixed
    {
        return $this->model;
    }
}