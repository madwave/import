<?php

namespace Madwave\Import\Services\ImportService;

use Carbon\Carbon;

enum ImportTypeEnum: string
{
    case ALL = 'all_resources';
    case ONLY_UPDATED = 'new_resources';

    /**
     * @return bool
     */
    public function withCheckingDelete(): bool
    {
        return match ($this) {
            ImportTypeEnum::ALL => config('import.types.all_resources.with_delete'),
            ImportTypeEnum::ONLY_UPDATED => config('import.types.new_resources.with_delete'),
        };
    }

    /**
     * @return string
     */
    public function runTime(): string
    {
        return match ($this) {
            ImportTypeEnum::ALL => Carbon::parse(config('import.types.all_resources.runtime'))
                ->format('H:i'),

            default => Carbon::now()->format('H:i')
        };
    }
}
