<?php

namespace Madwave\Import\Services\ImportService;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Madwave\Import\ApiContext;

/**
 * @mixin Builder
 */
abstract class ImportModel extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d h:i:s',
        'updated_at' => 'datetime:Y-m-d h:i:s',
        'deleted_at' => 'datetime:Y-m-d h:i:s'
    ];

    public array $idFieldNames = ['id'];

    /**
     * @param ApiContext $context
     * @param ...$params
     */
    abstract public function fillContext(ApiContext $context, ...$params): void;

    /**
     * @param ImportRepository $repository
     * @param ImportTypeEnum $type
     * @param array $resources
     * @return mixed
     */
    public function insertImportResources(ImportRepository $repository, ImportTypeEnum $type, array $resources): mixed
    {
        return $repository->importAsArray($resources, $type->withCheckingDelete());
    }

    public static function boot()
    {
        parent::boot();
        static::observe(ImportModelObserver::class);
    }

    public static function tableName() {
        return (new static)->getTable();
    }
}