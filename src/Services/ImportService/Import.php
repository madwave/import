<?php

namespace Madwave\Import\Services\ImportService;

use Madwave\Import\Services\ImportService\DataImportInfo\DataImportStorage;
use Madwave\Import\Services\ImportService\DataImportInfo\HistoryImportInfoResource;
use Madwave\Import\Services\ImportService\DataImportInfo\LastImportInfoResource;
use Madwave\Import\Models\ProductCategoryImport;
use Madwave\Import\Models\ProductVariantsImport;
use Madwave\Import\Models\ProductColorsImport;
use Madwave\Import\Models\ProductPriceImport;
use Madwave\Import\Models\ProductSizesImport;
use Madwave\Import\Models\ProductStockImport;
use Madwave\Import\Models\ProductTypeImport;
use Madwave\Import\Models\SizeDetailImport;
use Madwave\Import\Exceptions\ApiException;
use Madwave\Import\Models\CountryImport;
use Madwave\Import\Models\MeasureImport;
use Madwave\Import\Models\ProductImport;
use Madwave\Import\Models\GenderImport;
use Madwave\Import\Models\BrandImport;
use Madwave\Import\RequestClient;
use Madwave\Import\ApiContext;
use Carbon\Carbon;
use Exception;
use Madwave\Import\Models\ProductSizesSizeDetailsImport;

class Import
{
    protected DataImportStorage $importStorage;
    protected LastImportInfoResource $importInfo;
    protected HistoryImportInfoResource $importHistory;
    protected ?string $updatedAt;
    protected ?string $queue = null;
    protected $callback;
    protected $force = false;

    public ImportTypeEnum $type;

    /**
     * @param ImportTypeEnum $type
     * @param string|null $updatedAt
     */
    public function __construct(ImportTypeEnum $type, string $updatedAt = null)
    {
        $this->importStorage = new DataImportStorage($type);
        $this->importInfo = $this->importStorage->getResource(LastImportInfoResource::class);
        $this->importHistory = $this->importStorage->getResource(HistoryImportInfoResource::class);
        $this->updatedAt = $updatedAt;
        $this->type = $type;
    }

    /**
     * @param bool $force
     * @return bool
     */
    public function run(bool $force = false): bool
    {
        $this->force = $force;

        if (!$force && !$this->isRuntime() && $this->importInfo->isImportfinished()) {
            return false;
        }

        $this->continueOrNewImport();

        match ($this->type) {
            ImportTypeEnum::ALL => $this->dispatchImport(fn() => $this->importAll()),
            ImportTypeEnum::ONLY_UPDATED => $this->dispatchImport(fn() => $this->importOnlyUpdated()),
        };

        return true;
    }

    /**
     * @return void
     */
    private function continueOrNewImport(): void
    {
        if ($this->importInfo->isImportfinished()) {
            $this->importHistory->unshiftImport($this->importInfo);
            $this->importStorage->save($this->importHistory);
            $this->importInfo->refresh();
        }
    }

    /**
     * @param callable $importMethod
     * @return void
     */
    private function dispatchImport(callable $importMethod): void
    {
        $importMethod = function () use ($importMethod) {
            $importMethod();
            $this->importInfo->importFinished();
            $this->importStorage->save($this->importInfo);
        };

        $this->queue ? dispatch($importMethod)->onQueue($this->queue) : $importMethod();
    }

    private function importBaseModels(): void
    {
        $importClassesForImporting = [
            CountryImport::class,
            MeasureImport::class,
            BrandImport::class,
            ProductCategoryImport::class,
            GenderImport::class,
            ProductTypeImport::class,
            ProductColorsImport::class,
            SizeDetailImport::class,
        ];

        foreach ($importClassesForImporting as $importClass) {
            if ($this->isStageRuntime($importClass)) {
                $this->importModel(new $importClass());
            }
        }
    }

    /**
     * @return void
     * @throws ApiException
     * @throws \Throwable
     */
    private function importOnlyUpdated(): void
    {
        $this->updatedAt = $this->updatedAt ?: $this->importInfo->last_import_at;

        $this->importBaseModels();

        if ($this->isStageRuntime(ProductSizesImport::class)) {
            if ($sizeDetails = $this->importModel(new ProductSizesImport())) {
                $this->importModel(new ProductSizesSizeDetailsImport(), resources: $sizeDetails);
            }
        }

        if ($this->isStageRuntime(ProductImport::class)) {
            if ($productIds = $this->importModel(new ProductImport())) {
                $this->importModel(new ProductVariantsImport(), productIds: $productIds);
            }
        }

        if ($this->isStageRuntime(ProductStockImport::class)) {
            $this->importModel(new ProductStockImport());
        }
        if ($this->isStageRuntime(ProductPriceImport::class)) {
            $this->importModel(new ProductPriceImport());
        }
    }

    /**
     * @return void
     * @throws ApiException
     * @throws \Throwable
     */
    private function importAll(): void
    {
        //TODO:: сделать на поле deleted_at

        $this->importBaseModels();

        if ($this->isStageRuntime(ProductSizesImport::class)) {
            $sizeDetails = $this->importModel(new ProductSizesImport());

            if ($this->isStageRuntime(ProductSizesSizeDetailsImport::class)) {
                $this->importModel(new ProductSizesSizeDetailsImport(), resources: $sizeDetails);
            }
        }

        if ($this->isStageRuntime(ProductImport::class)) {
            $productVariants = $this->importModel(new ProductImport());
            if ($this->isStageRuntime(ProductVariantsImport::class)) {
                $this->importModel(new ProductVariantsImport(), resources: $productVariants);
            }
        }

        if ($this->isStageRuntime(ProductStockImport::class)) {
            $this->importModel(new ProductStockImport());
        }
        if ($this->isStageRuntime(ProductPriceImport::class)) {
            $this->importModel(new ProductPriceImport());
        }
    }

    /**
     * @param ImportModel $model
     * @param array ...$resources
     * @param mixed ...$params
     * @return mixed
     */
    private function importModel(ImportModel $model, array $resources = null, mixed ...$params): mixed
    {
        $this->importInfo->stageStarted($model::class);
        $this->importStorage->save($this->importInfo);

        try {
            $resources ??= $this->requestResources($model, $params);
            $result = $this->insertModel($model, $resources);
        } catch (ApiException $exception) {
            $this->importInfo->setSavedResources($exception->getResources());
            $this->importInfo->setLastQuery($exception->getRequestInfo());
            $this->importStorage->save($this->importInfo);

            throw $exception;
        } catch (\Throwable $exception) {
            if (!empty($resources)) {
                $this->importInfo->setSavedResources($resources);
            }

            $this->importStorage->save($this->importInfo);

            throw $exception;
        }

        $this->importInfo->stageFinished($model::class);
        $this->runCallback($model::class);

        return $result;
    }

    public function insertModel(ImportModel $model, array $resources): mixed
    {

        $repository = new ImportRepository($model, $this->importInfo);

        return $model->insertImportResources($repository, $this->type, $resources);
    }

    /**
     * @param ImportModel $model
     * @param array $params
     * @return array
     * @throws ApiException|Exception
     */
    private function requestResources(ImportModel $model, array $params): array
    {
        $context = new ApiContext();
        $context->setUpdatedAtUnix($this->updatedAt);
        $model->fillContext($context, ...$params);

        $query = $this->importInfo->getQueryIfSame($context->toArray());

        if ($query) {
            $fromPage = $query['params']['page'];
        }

        $savedResources = $this->importInfo->pullSavedResources();

        if (!empty($savedResources) && !$query) {
            return $savedResources;
        }

        return (new RequestClient($context))->getAllResources($savedResources, $fromPage ?? null);
    }

    /**
     * @param callable $callback
     * @return self
     */
    public function setCallback(callable $callback): self
    {
        $this->callback = $callback;

        return $this;
    }

    /**
     * @param ?string $queueName
     * @return $this
     */
    public function onQueue(string $queueName = null): self
    {
        $this->queue = $queueName;

        return $this;
    }

    /**
     * @param string $className
     * @return void
     */
    private function runCallback(string $className): void
    {
        if ($callback = $this->callback) {
            $callback($className);
        }
    }

    /**
     * @return bool
     */
    public function isRuntime(): bool
    {
        return $this->type->runTime() == Carbon::now()->format('H:i');
    }

    /**
     * @param string $stage
     * @return bool
     */
    public function isStageRuntime(string $stage): bool
    {
        if (!$this->force && $this->importInfo->isStageFinished($stage)) {
            $repeatTime = config('import.types.' . $this->type->value . '.repeat_time.' . $stage);

            return Carbon::now()->diffInSeconds($this->importInfo->getStage($stage)) >= $repeatTime;
        }

        return true;
    }
}