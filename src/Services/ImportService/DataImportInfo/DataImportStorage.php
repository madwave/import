<?php

namespace Madwave\Import\Services\ImportService\DataImportInfo;

use Madwave\Import\Services\ImportService\ImportTypeEnum;
use Illuminate\Support\Facades\Redis;
use stdClass;

/**
 * Информация об импорте данных
 */
class DataImportStorage
{
    public const REDIS_NAME = 'madwave.import.info';

    private ImportTypeEnum $type;

    /**
     * @param ImportTypeEnum $type
     */
    public function __construct(ImportTypeEnum $type)
    {
        $this->type = $type;
    }

    /**
     * @param AbstractImportInfoResource $resource
     * @return void
     */
    public function save(AbstractImportInfoResource $resource): void
    {
        Redis::set($this->getFullRedisKey($resource->resourceName()), $resource->toJson());
    }

    /**
     * @param string $resourceName
     * @return void
     */
    public function clear(string $resourceName): void
    {
        Redis::del($this->getFullRedisKey($resourceName));
    }

    /**
     * @param string $resourceName
     * @return stdClass|null
     */
    public function getFromRedis(string $resourceName): array|null
    {
        $redisValue = Redis::get($this->getFullRedisKey($resourceName));

        return $redisValue ? json_decode($redisValue, true) : null;
    }

    /**
     * @param string $resourceClass
     * @return LastImportInfoResource
     */
    public function getResource(string $resourceClass): AbstractImportInfoResource
    {
        return new $resourceClass($this->type, $this->getFromRedis($resourceClass::resourceName()));
    }

    public function getFullRedisKey(string $resourceName): string
    {
        return sprintf('%s.%s.%s', self::REDIS_NAME, $this->type->value, $resourceName);
    }
}
