<?php

namespace Madwave\Import\Services\ImportService\DataImportInfo;

use Illuminate\Bus\Queueable;
use Madwave\Import\Services\ImportService\ImportTypeEnum;

/**
 * Класс информации импорта
 */
class LastImportInfoResource extends AbstractImportInfoResource
{
    public array $added = [];
    public array $missed = [];
    public array $deleted = [];
    public array $query = [];
    public array $stages = [];
    public array $savedResources = [];
    public ImportTypeEnum $type;
    public string $import_at;
    public string $last_import_at;

    /**
     * @param ImportTypeEnum $type
     * @param $data
     */
    public function __construct(ImportTypeEnum $type, array $data = null)
    {
        $this->import_at = now();
        $this->last_import_at = $this->import_at;

        if ($data) {
            $this->savedResources = $data['savedResources'];
            $this->last_import_at = $data['last_import_at'];
            $this->import_at = $data['import_at'];
            $this->stages = $data['stages'];
            $this->query = $data['query'];
            $this->deleted = $data['deleted'];
            $this->missed = $data['missed'];
            $this->added = $data['added'];
        }

        parent::__construct($type, $data);
    }

    /**
     * @return void
     */
    public function refresh(): void
    {
        $this->import_at = now();
        $this->savedResources = [];
        $this->deleted = [];
        $this->missed = [];
        $this->added = [];
        $this->query = [];
    }

    public function importFinished()
    {
        if ($this->isImportfinished()) {
            $this->last_import_at = $this->import_at;
        }
    }

    /**
     * @param string $resource
     * @param array $ids
     * @return void
     */
    public function pushAdded(string $resource, array $ids): void
    {
        if (!empty($ids)) {
            $this->added[$resource][] = $ids;
        }
    }

    /**
     * @param string $resource
     * @param array $ids
     * @return void
     */
    public function pushDeleted(string $resource, array $ids): void
    {
        if (!empty($ids)) {
            $this->deleted[$resource][] = $ids;
        }
    }

    /**
     * @param string $resource
     * @param array $ids
     * @return void
     */
    public function pushMissed(string $resource, array $ids): void
    {
        if (!empty($ids)) {
            $this->missed[$resource][] = $ids;
        }
    }

    /**
     * @param string $stageName
     * @return void
     */
    public function stageFinished(string $stageName): void
    {
        $this->stages[$stageName] = $this->import_at;
        $this->savedResources = [];
        $this->query = [];
    }

    /**
     * @param string $stageName
     * @return string|false
     */
    public function getStage(string $stageName): string|false
    {
        return $this->stages[$stageName] ?? false;
    }

    /**
     * @param string $stageName
     * @return void
     */
    public function stageStarted(string $stageName): void
    {
        $this->stages[$stageName] = false;
    }

    /**
     * @param string $stageName
     * @return bool
     */
    public function isStageFinished(string $stageName): bool
    {
        return $this->stages[$stageName] ?? false;
    }

    /**
     * @return bool
     */
    public function isImportfinished(): bool
    {
        return !in_array(false, $this->stages, true);
    }

    /**
     * @param array $query
     * @return void
     */
    public function setLastQuery(array $query): void
    {
        $this->query = $query;
    }

    /**
     * @param array $savingResources
     * @return void
     */
    public function setSavedResources(array $savingResources): void
    {
        $this->savedResources = $savingResources;
    }

    /**
     * @return array
     */
    public function pullSavedResources(): array
    {
        $resources = $this->savedResources;
        $this->savedResources = [];

        return $resources;
    }

    /**
     * @param array $query
     * @return ?array
     */
    public function getQueryIfSame(array $query): ?array
    {
        if (empty($query['url']) || empty($query['method'])) {
            return null;
        }

        if ($this->query && $this->query['url'] === $query['url'] && $this->query['method'] === $query['method']) {
            return $this->query;
        }

        return null;
    }

    /**
     * @return string
     */
    public static function resourceName(): string
    {
        return 'last';
    }
}
