<?php

namespace Madwave\Import\Services\ImportService\DataImportInfo;

use Madwave\Import\Services\ImportService\ImportTypeEnum;

abstract class AbstractImportInfoResource
{
    public ImportTypeEnum $type;

    public function __construct(ImportTypeEnum $type, array $data = null)
    {
        $this->type = $type;
    }

    abstract public static function resourceName(): string;

    /**
     * @return string
     */
    public function toJson(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}