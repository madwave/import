<?php

namespace Madwave\Import\Services\ImportService\DataImportInfo;

use Illuminate\Bus\Queueable;
use Madwave\Import\Services\ImportService\ImportTypeEnum;

/**
 * Класс информации импорта
 */
class HistoryImportInfoResource extends AbstractImportInfoResource
{
    public array $history = [];

    /**
     * @param ImportTypeEnum $type
     * @param array|null $data
     */
    public function __construct(ImportTypeEnum $type, array $data = null)
    {
        if ($data) {
            $this->history = $data['history'];
        }

        parent::__construct($type, $data);
    }

    /**
     * @param LastImportInfoResource $info
     * @return void
     */
    public function unshiftImport(LastImportInfoResource $info): void
    {
        array_unshift($this->history, [
            'added' => $info->added,
            'deleted' => $info->deleted,
            'missed' => $info->missed,
            'import_at' => $info->last_import_at
        ]);

        $this->history = array_splice($this->history, 0, config('import.max_prev_import_number'));
    }

    public static function resourceName(): string
    {
        return 'history';
    }
}
