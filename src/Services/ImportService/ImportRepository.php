<?php

namespace Madwave\Import\Services\ImportService;

use Madwave\Import\Services\ImportService\DataImportInfo\LastImportInfoResource;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;
use Exception;

/**
 * Репозиторий импорта данных
 */
class ImportRepository
{
    private ImportModel $model;
    private LastImportInfoResource $importInfo;

    /**
     * @param ImportModel $model
     * @param LastImportInfoResource $importInfo
     */
    public function __construct(ImportModel $model, LastImportInfoResource $importInfo)
    {
        $this->importInfo = $importInfo;
        $this->setNewModel($model);
    }

    /**
     * @param ImportModel $model
     * @return void
     */
    public function setNewModel(ImportModel $model): void
    {
        $this->model = $model;
    }

    /**
     * Получить поля в виде массива
     * @param mixed $importData
     * @return array
     */
    protected function getIdFieldsArray(array $importData): array
    {
        $idFieldsArray = [];

        foreach ($this->model->idFieldNames as $idFieldName) {
            $idFieldsArray[$idFieldName] = $importData[$idFieldName];
        }

        return $idFieldsArray;
    }

    /**
     * @param array $importObjects
     * @param bool $withDelete
     * @return array[]
     */
    public function importAsArray(array $importObjects, bool $withDelete = false): array
    {
        $addedFieldsIds = [];
        $dontAddedFieldsIds = [];

        foreach ($importObjects as $importObject) {
            $importArray = (array)$importObject;
            $isAdded = $this->import($importArray);

            if ($isAdded) {
                $addedFieldsIds[] = $this->getIdFieldsArray($importArray);
            } else {
                $dontAddedFieldsIds[] = $this->getIdFieldsArray($importArray);
            }
        }

        if ($withDelete) {
            $this->checkingAllItemsForDelete(array_merge($addedFieldsIds, $dontAddedFieldsIds));
        }

        return ['added' => $addedFieldsIds, 'notAdded' => $dontAddedFieldsIds];
    }

    /**
     * Импорт данных
     * @param array $importArray
     * @return bool
     */
    private function import(array $importArray): bool
    {
        $importArray = $this->formatImportArray($importArray);
        $idFieldsArray = $this->getIdFieldsArray($importArray);

        try {

            if (in_array(SoftDeletes::class, class_uses($this->model))) {
                $model = $this->model->withTrashed()->updateOrCreate($idFieldsArray, $importArray);
            } else {
                $model = $this->model->updateOrCreate($idFieldsArray, $importArray);
            }

            if ($model->wasChanged() || $model->wasRecentlyCreated) {
                $this->importInfo->pushAdded($this->model::class, $model->only($this->model->idFieldNames));
                return true;
            }

           $this->importInfo->pushMissed($this->model::class, $model->only($this->model->idFieldNames));

            return false;

        } catch (Exception $e) {
            $message = sprintf('[importData %s] Error - %s', implode($idFieldsArray), $e->getMessage());

            Log::info($message);

            return false;
        }
    }

    /**
     * @param array $allFieldsIds
     * @return void
     */
    private function checkingAllItemsForDelete(array $allFieldsIds): void
    {
        try {
            $query = $this->model->query();

            foreach ($this->model->idFieldNames as $fieldName) {
                $query->whereNotIn($fieldName, array_column($allFieldsIds, $fieldName));
            }

            $ids = $query->select($this->model->idFieldNames)->get()->toArray();

            $query->delete();

            $this->importInfo->pushDeleted($this->model::class, $ids);

        } catch (Exception $e) {
            $message = sprintf(
                '[deleteAfterImport %s] Error - %s',
                implode($this->model->idFieldNames),
                $e->getMessage()
            );

            Log::info($message);
        }
    }

    /**
     * @param array $importArray
     * @return array
     */
    private function formatImportArray(array $importArray): array
    {
        if (!empty($importArray['updated_at'])) {
            $importArray['updated_at'] = Carbon::parse($importArray['updated_at'])->format('Y-m-d H:i:s');
        }
        $importArray['deleted_at'] = null;

        return $importArray;
    }
}
