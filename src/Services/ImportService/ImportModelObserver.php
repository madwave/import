<?php

namespace Madwave\Import\Services\ImportService;

use Madwave\Import\Events\CreatedModelEvent;

class ImportModelObserver
{

    /**
     * @param ImportModel $model
     * @return void
     */
    public function created(ImportModel $model): void
    {
        event(new CreatedModelEvent($model));
    }
}
