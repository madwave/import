<?php

namespace Madwave\Import;

use Madwave\Import\Console\MadwaveImport;
use Madwave\Import\Console\MadwaveInit;
use Illuminate\Support\ServiceProvider;

class MadwaveImportProvider extends ServiceProvider
{
    /**
     * Register any application services.
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->commands([MadwaveInit::class, MadwaveImport::class]);
        $this->loadRoutesFrom(__DIR__ .'/Routes/Import.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'import');
    }
}
